<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Stock Search</title>
    <style>
        #search_container {
            margin: 0 auto;
            width: 460px;
            border: 1px solid #d9d9d9;
            background-color: #f2f2f2;
        }

        .search_header {
            width: 450px;
            margin: 0 auto;
            border-bottom: 1px solid #d9d9d9;
        }

        .search_header p {
            font-size: 30px;
            font-style: italic;
            text-align: center;
            margin-top: 5px;
            margin-bottom: 5px;
            font-weight: bold;
        }

        .search_box {
            padding-left: 10px;
            margin-bottom: 5px;
            font-size: 16px;
        }

        .search_box input[type=text] {
            width: 140px;
            height: 13px;
            font-size: 11px;
        }

        .search_control {
            padding-left: 194px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .search_control input[type=submit] {
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            border: 1px solid #d9d9d9;
            padding: 1px 7px;
            display: inline-block;
            text-decoration: none;
            background: #ffffff;
            cursor: pointer;
        }

        .disclaimer {
            margin-top: 5px;
            margin-bottom: 25px;
            padding-left: 136px;
            font-size: 16px;
        }

        #company_content, #company_detail {
            width: 680px;
            border: 2px solid #c7c7c7;
            font-family: "Arial";
            border-collapse: collapse;
            margin: 10px auto 0 auto;
        }

        #company_content td, th {
            border: 1px solid #c7c7c7;
        }

        #company_detail td, th {
            border: 1px solid #c7c7c7;
        }

        #company_content, #company_detail th {
            background-color: #f0f0f0;
            text-align: left;
            height: 25px;
        }

        #company_content th.detail {
            width: 11%;
        }

        #company_detail th {
            width: 50%;
        }

        #company_content td {
            background-color: #fafafa;
            height: 25px;
        }

        #company_detail td {
            text-align: center;
            background-color: #fafafa;
            height: 25px;
        }

        img.thumbnail_size {
            height: 15px;
            width: 15px;
        }

        [required] {
            box-shadow: none;
        }
    </style>

    <script type="application/javascript">
        function clearContent() {
            document.getElementById('search_input').required = false;
//            document.getElementById('search_form').noValidate = true;
            document.getElementById('search_input').value = '';
            try {
                document.getElementById('company_content').style.visibility = 'hidden';
            } catch (e) {}
            try {
                document.getElementById('company_detail').style.visibility = 'hidden';
            } catch (e) {}
            document.getElementById('search_input').required = true;
//            document.getElementById('search_form').noValidate = false;
        }
    </script>
</head>
<body>
<div id="search_container">
    <form action="stock.php" method="get" id="search_form">
        <div class="search_header">
            <p>Stock Search</p>
        </div>
        <p class="search_box">Company Name or Symbol: <input type="text" id="search_input" name="input" value="<?php if(isset($_GET['input'])) echo $_GET['input']; ?>" pattern="[a-zA-Z0-9\s]*[a-zA-Z0-9]+[a-zA-Z0-9\s]*" required title="Please enter Name or Symbol" x-moz-errormessage="Please enter Name or Symbol"></p>
    </form>
    <p class="search_control">
        <input type="submit" value="Search" form="search_form">
        <input type="submit" value="Clear" onclick="clearContent();">
    </p>
    <p class="disclaimer"><a href="http://www.markit.com/product/markit-on-demand" target="_blank">Powered by Markit on Demand</a></p>
</div>

<?php
if(isset($_GET['symbol'])) {
    displayCompanyDetails($_GET['symbol']);
} else if(isset($_GET['input'])) {
    displayCompanyList(trim($_GET['input'], " \t\n\r\0\x0B"));
}

function displayCompanyList($input) {
    if($input == null || !isset($input)) {
        return;
    }
    //construct Lookup API URL
    $baseUrl = "http://dev.markitondemand.com/MODApis/Api/v2/Lookup/xml";
    $params = array('input' => $input);
    $url = sprintf("%s?%s", $baseUrl, http_build_query($params));
    set_error_handler("warning_handler", E_WARNING);
    $lookupXml = simplexml_load_file($url);
//        echo "<script>alert('Failed to retrieve Lookup data from server. Please try again.');</script>";
//        return;
//    }
    restore_error_handler();
    ?>
    <table id="company_content">
        <tr>
            <?php
            if($lookupXml->children()->count() == 0) {
                echo "<td style='text-align: center;'>No Record has been found</td></tr></table>";
                return;
            }
            ?>
            <th>Name</th>
            <th>Symbol</th>
            <th>Exchange</th>
            <th class="detail">Details</th>
        </tr>

        <?php
        /*try {
            $companiesList = $lookupXml->LookUpResult;
        } catch(Exception $e) {
            */?><!--
        <script>alert("Cannot get Companies List.");</script>
        --><?php
        /*        die();
            }*/

        $localBaseUrl = "stock.php";
        foreach($lookupXml->children() as $company) {
            $params = array('input' => $input, 'symbol' => (string)$company->Symbol);
            $infoUrl = sprintf("%s?%s", $localBaseUrl, http_build_query($params));
            ?>
            <tr>
                <td><?php echo $company->Name; ?></td>
                <td><?php echo $company->Symbol; ?></td>
                <td><?php echo $company->Exchange; ?></td>
                <td><a href="<?php echo $infoUrl; ?>">More Info</a></td>
            </tr>
        <?php
        }
        ?>
    </table>
<?php
}

function displayCompanyDetails($symbol) {
    if($symbol == null || !isset($symbol)) {
        return;
    }
    //construct Lookup API URL
    $baseUrl = "http://dev.markitondemand.com/MODApis/Api/v2/Quote/json";
    $params = array('symbol' => $symbol);
    $url = sprintf("%s?%s", $baseUrl, http_build_query($params));
//    $companyDetailStr = executeCurl($url);
    set_error_handler("warning_handler", E_WARNING);
    $companyDetailStr = file_get_contents($url);
    restore_error_handler();
    $companyDetailJson = json_decode($companyDetailStr);
    if(!isset($companyDetailJson->Status)) {
        echo "<script>alert('Invalid Symbol');</script>";
        die();
    }
    ?>
    <table id="company_detail">
        <tr>
            <?php
            if(preg_match('/^failure/', strtolower($companyDetailJson->Status))) {
                echo "<td style='text-align: center;'>There is no stock information available</td></tr></table>";
                return;
            }
            ?>
            <th>Name</th>
            <td><?php echo $companyDetailJson->Name; ?></td>
        </tr>
        <tr>
            <th>Symbol</th>
            <td><?php echo $companyDetailJson->Symbol; ?></td>
        </tr>
        <tr>
            <th>Last Price</th>
            <td><?php echo $companyDetailJson->LastPrice; ?></td>
        </tr>
        <tr>
            <th>Change</th>
            <td><?php echo round($companyDetailJson->Change, 2); showMarker($companyDetailJson->Change);?></td>
        </tr>
        <tr>
            <th>Change Percent</th>
            <td><?php echo round($companyDetailJson->ChangePercent, 2) . "%"; showMarker($companyDetailJson->ChangePercent);?></td>
        </tr>
        <tr>
            <th>Timestamp</th>
            <td><?php
                $dateObj = new DateTime($companyDetailJson->Timestamp);
                echo $dateObj->format('Y-m-d h:i A') . " (EST)";?>
            </td>
        </tr>
        <tr>
            <th>Market Cap</th>
            <td><?php displayMarketCap($companyDetailJson->MarketCap); ?></td>
        </tr>
        <tr>
            <th>Volume</th>
            <td><?php echo number_format($companyDetailJson->Volume, 0 , '.' , ','); ?></td>
        </tr>
        <tr>
            <th>Change YTD</th>
            <td><?php
                $changeYTD = $companyDetailJson->LastPrice - $companyDetailJson->ChangeYTD;
                if($changeYTD < 0) {
                    echo "(" . round($changeYTD, 2) . ")";
                } else {
                    echo round($changeYTD, 2);
                }
                showMarker($changeYTD);?>
            </td>
        </tr>
        <tr>
            <th>Change Percent YTD</th>
            <td><?php echo round($companyDetailJson->ChangePercentYTD, 2) . "%"; showMarker($companyDetailJson->ChangePercentYTD); ?></td>
        </tr>
        <tr>
            <th>High</th>
            <td><?php echo $companyDetailJson->High; ?></td>
        </tr>
        <tr>
            <th>Low</th>
            <td><?php echo $companyDetailJson->Low; ?></td>
        </tr>
        <tr>
            <th>Open</th>
            <td><?php echo $companyDetailJson->Open; ?></td>
        </tr>
    </table>
<?php

}
/*function executeCurl($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}*/

function showMarker($value) {
    if($value > 0) {
        echo " <img src='http://cs-server.usc.edu:45678/hw/hw6/images/Green_Arrow_Up.png' class='thumbnail_size'>";
    } else if($value < 0) {
        echo " <img src='http://cs-server.usc.edu:45678/hw/hw6/images/Red_Arrow_Down.png' class='thumbnail_size'>";
    }
}

function displayMarketCap($marketCap) {
    $formattedMarketCap = $marketCap/1000000000;
    if($formattedMarketCap < 0.01) {
        $formattedMarketCap = $marketCap/1000000;
        echo round($formattedMarketCap, 2) . " M";
    } else {
        echo round($formattedMarketCap, 2) . " B";
    }
}

function warning_handler($errno, $errstr) {
    echo "<script>alert('Failed to retrieve Lookup data from server. Please try again.');</script>";
    die();
}
?>
</body>
</html>
<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if(isset($_GET['lookup'])) {
    $input = $_GET['lookup'];
    if($input == null || $input == '') {
        return;
    }
    $input = trim($input, " \t\n\r\0\x0B");
    //construct Lookup API URL
    $baseUrl = "http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json";
    $params = array('input' => $input);
    $url = sprintf("%s?%s", $baseUrl, http_build_query($params));
    $companyListStr = file_get_contents($url);
    $companyListArr = json_decode($companyListStr);
    $formattedCompanyList = [];
    foreach($companyListArr as $company) {
        if($company->Symbol && $company->Name && $company->Exchange) {
            $tempCompany = $company->Symbol . " - " . $company->Name . " ( " . $company->Exchange . " )";
            $formattedCompanyList[] = $tempCompany;
        }
    }
    echo json_encode($formattedCompanyList);
}

if(isset($_GET['details'])) {
    $input = $_GET['details'];
    if($input == null || $input == '') {
        return;
    }
    $input = trim($input, " \t\n\r\0\x0B");
    $baseUrl = "http://dev.markitondemand.com/MODApis/Api/v2/Quote/json";
    $params = array('symbol' => $input);
    $url = sprintf("%s?%s", $baseUrl, http_build_query($params));
    set_error_handler("warning_handler", E_WARNING);
    $companyDetailJson = array();
    while(1) {
        $companyDetailStr = file_get_contents($url);
        if (preg_match('/^blocked/', strtolower($companyDetailStr))) {
            header('HTTP/1.1 408 Request Timeout', true, 408);
            return;
        }
        $companyDetailJson = json_decode($companyDetailStr);
        //check if error
        if(isset($companyDetailJson->Message)) {
            header('HTTP/1.1 400 Bad Request', true, 400);
            return;
        }
        if(preg_match('/^failure/', strtolower($companyDetailJson->Status))) {
            header('HTTP/1.1 404 Not Found', true, 404);
            return;
        }

        if($companyDetailJson->Symbol != null) {
            break;
        }
    }

    //modify individual items as per specifications
    $companyDetailJson->LastPrice = number_format(round($companyDetailJson->LastPrice, 2), 2);
    $companyDetailJson->Change = number_format(round($companyDetailJson->Change, 2), 2);
    $companyDetailJson->ChangePercent = number_format(round($companyDetailJson->ChangePercent, 2), 2);
    $dateObj = new DateTime($companyDetailJson->Timestamp);
    $dateObj->setTimezone(new DateTimeZone("America/Los_Angeles"));
    $companyDetailJson->Timestamp = $dateObj->format('d F Y, h:i:s a');
    $companyDetailJson->MarketCap = formatMarketCap($companyDetailJson->MarketCap);
    $companyDetailJson->ChangeYTD = number_format(round($companyDetailJson->ChangeYTD, 2), 2);
    $companyDetailJson->ChangePercentYTD = number_format(round($companyDetailJson->ChangePercentYTD, 2), 2);
    $companyDetailJson->High = number_format(round($companyDetailJson->High, 2), 2);
    $companyDetailJson->Low = number_format(round($companyDetailJson->Low, 2), 2);
    $companyDetailJson->Open = number_format(round($companyDetailJson->Open, 2), 2);

    echo json_encode($companyDetailJson);
}

if(isset($_GET['favorite_details'])) {
    $input = $_GET['favorite_details'];
    if($input == null || $input == '') {
        return;
    }
    $input = trim($input, " \t\n\r\0\x0B");
    $baseUrl = "http://dev.markitondemand.com/MODApis/Api/v2/Quote/json";
    $params = array('symbol' => $input);
    $url = sprintf("%s?%s", $baseUrl, http_build_query($params));
    $companyDetailJson = array();
    while(1) {
        $companyDetailStr = file_get_contents($url);
        $companyDetailJson = json_decode($companyDetailStr);

        if ($companyDetailJson->Symbol != null) {
            break;
        }
    }

    $favoriteDetailJson = array(
        'Symbol' => $companyDetailJson->Symbol,
        'Name' => $companyDetailJson->Name,
        'LastPrice' => number_format(round($companyDetailJson->LastPrice, 2), 2),
        'Change' => number_format(round($companyDetailJson->Change, 2), 2),
        'ChangePercent' => number_format(round($companyDetailJson->ChangePercent, 2), 2),
        'MarketCap' => formatMarketCap($companyDetailJson->MarketCap)
    );

    echo json_encode($favoriteDetailJson);
}

if(isset($_GET['get_news'])) {
    $input = $_GET['get_news'];
    if($input == null || $input == '') {
        return;
    }
    $accountKey = '522KMeAbkg82+zp5kPsHkF24yClpgOXXpKM1DddX0r4';
    $context = stream_context_create(array(
        'http' => array(
            'request_fulluri' => true,
            'header'  => "Authorization: Basic " . base64_encode($accountKey . ":" . $accountKey)
        )
    ));
    $bingNewsRequestURL =  'https://api.datamarket.azure.com/Bing/Search/v1/News?';
    $bingNewsRequestURL = $bingNewsRequestURL . '$format=json&Query=';
    $request = $bingNewsRequestURL . urlencode("'$input'");
    $newsResponse = json_decode(file_get_contents($request, 0, $context), true);
    $formattedNewsResponse = [];
    foreach($newsResponse['d']['results'] as $newsIterator) {
        $dateObj = new DateTime($newsIterator['Date']);
        $dateObj->setTimezone(new DateTimeZone("America/Los_Angeles"));
        $formattedNews = array(
            'title' => $newsIterator['Title'],
            'url' => $newsIterator['Url'],
            'publisher' => $newsIterator['Source'],
            'description' => $newsIterator['Description'],
            'date' => $dateObj->format('d M Y H:i:s')
        );
        $formattedNewsResponse[] = $formattedNews;
    }
    echo json_encode($formattedNewsResponse);
}

if(isset($_GET['get_interactive_chart'])) {
    $input = $_GET['get_interactive_chart'];
    if($input == null || $input == '') {
        return;
    }
    $input = trim($input, " \t\n\r\0\x0B");
    $MODInteractiveChartOptions = (object) array(
        'Normalized' => false,
        'NumberOfDays' => 1095,
        'DataPeriod' => "Day",
        'Elements' => array(
            array(
                'Symbol' => $input,
                'Type' => "price",
                'Params' => array('ohlc')
            )
        )
    );

    $url = "http://dev.markitondemand.com/MODApis/Api/v2/InteractiveChart/json?parameters=" . json_encode($MODInteractiveChartOptions);
    $chartData = array();
    while(1) {
        $chartData = file_get_contents($url);
        $chartDataJson = json_decode($chartData);
        if($chartDataJson->Positions != null) {
            break;
        }
    }
    echo $chartData;
}


function formatMarketCap($marketCap) {
    $formattedMarketCap = $marketCap/1000000000;
    if($formattedMarketCap > 1) {
        return round($formattedMarketCap, 2) . " Billion";
    }
    $formattedMarketCap = $marketCap/1000000;
    if($formattedMarketCap > 1) {
        return round($formattedMarketCap, 2) . " Million";
    }
    return $marketCap;
}

function warning_handler($errno, $errstr) {
    header('HTTP/1.1 408 Request Timeout', true, 408);
    die();
}
/**
 * Created by Ameya on 3/19/2016.
 */

/* Local API's */
/*const MODLookup = "php/index.php?lookup=";
const MODDetails = "php/index.php?details=";
const MODFavoriteDetails = "php/index.php?favorite_details=";
const BingGetNews = "php/index.php?get_news=";
const MODInteractiveChart = "php/index.php?get_interactive_chart=";*/

/* Production API's */
const MODLookup = "http://webtech-stock-details.appspot.com/?lookup=";
 const MODDetails = "http://webtech-stock-details.appspot.com/?details=";
 const MODFavoriteDetails = "http://webtech-stock-details.appspot.com/?favorite_details=";
 const BingGetNews = "http://webtech-stock-details.appspot.com/?get_news=";
 const MODInteractiveChart = "http://webtech-stock-details.appspot.com/?get_interactive_chart=";

/* Global Variables */
var selectedCompany = '';
var selectedCompanyDetails = {
    LastPrice: '',
    Name: '',
    Change: '',
    Image: '',
    HistoricalChartData: ''
};
var enableCarouselSwitch = false;

/* AutoComplete Functionality */
$('#companySearch').autocomplete({
    source: function( request, response ) {
        selectedCompany = '';
        var options = {
            type: "GET",
            url: MODLookup + $("#companySearch").val()
        };
        function selectSuccess(successResponse) {
            response(successResponse);
        }

        function selectFailure(failureResponse) {

        }
        __makeAjaxRequest(options, selectSuccess, selectFailure);
    },
    minLength: 1,
    select: function (event, selection) {
        event.preventDefault();
        selectedCompany = selection.item.value.slice(0, selection.item.value.indexOf(" -"));
        $("#companySearch").val(selectedCompany);
        $("#invalid-input").css('visibility', 'hidden');
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
});

/* Company Stock Details */
function getCompanyDetails(company) {
    var tempSelection = '';
    if(company) {
        selectedCompany = company;
        var options = {
            type: "GET",
            url: MODDetails + selectedCompany
        };
    } else {
        enableCarouselSwitch = true;
        tempSelection = $("#companySearch").val();
        var options = {
            type: "GET",
            url: MODDetails + tempSelection
        };
    }

    function detailsSuccess(companyDetails) {
        selectedCompany = companyDetails.Symbol;
        $("#invalid-input").css('visibility', 'hidden');

        $("#companyName").html(companyDetails.Name);
        $("#companySymbol").html(companyDetails.Symbol);
        $("#companyLastPrice").html("$ " + companyDetails.LastPrice);
        $("#companyChange").html(companyDetails.Change + " ( " + companyDetails.ChangePercent + "% )");
        displayMarker($("#companyChange"), '');
        $("#companyDateTime").html(companyDetails.Timestamp);
        $("#companyMarketCap").html(companyDetails.MarketCap);
        $("#companyVolume").html(companyDetails.Volume);
        $("#companyChangeYTD").html(companyDetails.ChangeYTD + " ( " + companyDetails.ChangePercentYTD + "% )");
        displayMarker($("#companyChangeYTD"), 'YTD');
        $("#companyHighPrice").html("$ " + companyDetails.High);
        $("#companyLowPrice").html("$ " + companyDetails.Low);
        $("#companyOpenPrice").html("$ " + companyDetails.Open);

        if(enableCarouselSwitch) {
            $("#switchCarouselButton").removeAttr("disabled");
        }
        $("#stockCarousel").carousel(1);

        $("#stockDetailsChart").attr("src", "http://chart.finance.yahoo.com/t?s=" + selectedCompany + "&lang=en-US&width=520&height=350");

        if(isFavorite(selectedCompany)) {
            $("#favorite-button").css("color", "yellow");
        } else {
            $("#favorite-button").css("color", "#ffffff");
        }

        selectedCompanyDetails = {
            Name: companyDetails.Name,
            LastPrice: "$ " + companyDetails.LastPrice,
            Change: companyDetails.Change + " ( " + companyDetails.ChangePercent + "% )",
            Image: "http://chart.finance.yahoo.com/t?s=" + selectedCompany + "&lang=en-US&width=200&height=200",
            HistoricalChartData: false
        };

        //displayHistoricalChart();

        var carouselData = $("#stockCarousel").data('bs.carousel');
        if(carouselData.getItemIndex(carouselData.$element.find('.item.active')) == 1) {
            if($("ul.nav-pills li.active")[0].id == "historical_charts_tab") {
                displayHistoricalChart();
            }
        } else {
            $("#display_historical_chart").html("");
        }

        getNewsFeed();

        function displayMarker(selector, YTD) {
            if(companyDetails['Change' + YTD]<0 || companyDetails['ChangePercent' + YTD]<0) {
                selector.css('color', 'red').append("<img class='profit-loss-thumbnail' src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png'>");
            } else if(companyDetails['Change' + YTD]>0 || companyDetails['ChangePercent' + YTD]>0){
                selector.css('color', 'green').append("<img class='profit-loss-thumbnail' src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png'>");
            } else {
                selector.css('color', '#000000');
            }
        }
    }

    function detailsFailure(failureResponse) {
        if(failureResponse.status == 400) {
            $("#invalid-input").css('visibility', 'visible');
        } else if(failureResponse.status == 404) {
            alert("No stock information available.");
        } else if(failureResponse.status == 408) {
            alert("Failed to retrieve stock details from server. Please try again.");
        }
    }
    __makeAjaxRequest(options, detailsSuccess, detailsFailure);
}

/* Historical Chart */
function displayHistoricalChart() {
    if(!selectedCompanyDetails.HistoricalChartData) {
        $.getJSON(MODInteractiveChart + selectedCompany, function (data) {
            var formattedData = formatInteractiveChartData(data);
            selectedCompanyDetails.HistoricalChartData = formattedData;
            renderHistoricalChart(formattedData);
        });
    } else {
        renderHistoricalChart(selectedCompanyDetails.HistoricalChartData);
    }


    function renderHistoricalChart(formattedData) {
        $('#display_historical_chart').highcharts('StockChart', {
            rangeSelector : {
                selected : 0,
                inputEnabled: false,
                allButtonsEnabled: true,
                buttons: [
                    {
                        type: 'week',
                        count: 1,
                        text: '1w'
                    }, {
                        type: 'month',
                        count: 1,
                        text: '1m'
                    }, {
                        type: 'month',
                        count: 3,
                        text: '3m'
                    }, {
                        type: 'month',
                        count: 6,
                        text: '6m'
                    }, {
                        type: 'ytd',
                        text: 'YTD'
                    }, {
                        type: 'year',
                        count: 1,
                        text: '1y'
                    }, {
                        type: 'all',
                        text: 'All'
                    }
                ]
            },
            yAxis: {
                title: {
                    text: 'Stock Value'
                },
                tickInterval: 1
            },
            title : {
                text : selectedCompany + ' Stock Price'
            },
            exporting: {
                enabled: false
            },
            series : [{
                name : selectedCompany,
                data : formattedData,
                type : 'area',
                threshold : null,
                tooltip : {
                    valueDecimals : 2,
                    valuePrefix: '$'
                },
                fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                }
            }]
        });
    }

}

/* Get Stock News */
function getNewsFeed() {
    //clear existing news
    $("#news_feed").html("");

    var options = {
        type: "GET",
        url: BingGetNews + selectedCompany
    };
    function selectSuccess(newsResponse) {
        if(newsResponse == [] || newsResponse == '') {
            $("#news_feed").html("<p>No News Available.</p>");
            return;
        }
        newsResponse.forEach(function(news) {
            var displayNews = "<div class='well'>" +
                "<p><a href='" + news.url + "' target='_blank'>" + news.title + "</a></p>" +
                "<p class='news_description'>" + news.description.replace(selectedCompany, "<b>" + selectedCompany + "</b>") + "</p>" +
                "<p class='bold'>Publisher: " + news.publisher + "</p>" +
                "<p class='bold'>Date: " + news.date + "</p>" +
                "</div>";
            $("#news_feed").append(displayNews);
        });
    }

    function selectFailure(failureResponse) {

    }
    __makeAjaxRequest(options, selectSuccess, selectFailure);
}

/* Util Functions */
/* AJAX framework */
var __makeAjaxRequest = function(options, successCallback, failureCallback){
    return $.ajax(options)
        .done(successCallback)
        .fail(failureCallback);
};

function isFavorite(company) {
    if(localStorage.favorites) {
        var favorites = localStorage.favorites.split(",");
        if(favorites.indexOf(company) != -1) {
            return true;
        }
    }
    return false;
}

function removeFavorite(company) {
    if(localStorage.favorites) {
        var favorites = localStorage.favorites.split(",");
        var companyIndex = favorites.indexOf(company);
        if(companyIndex != -1) {
            favorites.splice(companyIndex, 1);
            localStorage.favorites = favorites.join();
        }
    }
}

function fetchFavoriteDetail(company, detailsResponse) {
    if(company == '') {
        return;
    }
    var options = {
        type: "GET",
        url: MODFavoriteDetails + company
    };
    function favoriteDetailsSuccess(favoriteCompanyDetails) {
        detailsResponse(favoriteCompanyDetails);
    }

    function favoriteDetailsFailure(failureResponse) {

    }
    __makeAjaxRequest(options, favoriteDetailsSuccess, favoriteDetailsFailure);
}

function addFavorite(company) {
    if(localStorage.favorites) {
        var favorites = localStorage.favorites.split(",");
        favorites.push(company);
        localStorage.favorites = favorites.join();
    } else {
        localStorage.favorites = company;
    }
}

function getFavorites() {
    if(localStorage.favorites) {
        return localStorage.favorites.split(",");
    } else {
        return [];
    }
}

function displayFavoritesCarousel() {
    $("#favorite-table").find("tr:gt(0)").remove();
    displayFavorites();
    $('#stockCarousel').carousel('prev');
}

function refreshFavoriteDetails() {
    var favorites = getFavorites();
    favorites.forEach(function (company) {
        fetchFavoriteDetail(company, function (details) {
            $("#lastPrice-favorite-" + details.Symbol).html("$ " + details.LastPrice);

            var changeData = '';
            if (details.Change < 0 || details.ChangePercent < 0) {
                changeData = details.Change + " ( " + details.ChangePercent + "% )<img class='profit-loss-thumbnail' src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png'>";
                $("#change-favorite-" + details.Symbol).css("color", "#ff0000").html(changeData);
            } else if (details.Change > 0 || details.ChangePercent > 0) {
                changeData = details.Change + " ( " + details.ChangePercent + "% )<img class='profit-loss-thumbnail' src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png'>";
                $("#change-favorite-" + details.Symbol).css("color", "green").html(changeData);
            } else {
                changeData = details.Change + " ( " + details.ChangePercent + "% )";
                $("#change-favorite-" + details.Symbol).html(changeData);
            }

            $("#marketCap-favorite-" + details.Symbol).html(details.MarketCap);
        });
    });
}

function autoRefresh() {
    setInterval(function () {
        var carouselData = $("#stockCarousel").data('bs.carousel');
        if($("#auto-refresh-stock").is(":checked") && carouselData.getItemIndex(carouselData.$element.find('.item.active')) == 0) {
            refreshFavoriteDetails();
        }
    },5000);
}


function formatInteractiveChartData(data) {
    var dates = data.Dates || [];
    var elements = data.Elements || [];
    var chartSeries = [];
    if(elements[0]){
        for (var i = 0, datLen = dates.length; i < datLen; i++) {
            var dat = formatDate(dates[i]);
            var pointData = [
                dat,
                elements[0].DataSeries['open'].values[i],
                elements[0].DataSeries['high'].values[i],
                elements[0].DataSeries['low'].values[i],
                elements[0].DataSeries['close'].values[i]
            ];
            chartSeries.push( pointData );
        }
    }
    return chartSeries;
}

function formatDate(dateIn) {
    var dat = new Date(dateIn);
    return Date.UTC(dat.getFullYear(), dat.getMonth(), dat.getDate());
}


/* Views */

$(document).ready(function() {
    if(!enableCarouselSwitch) {
        $("#switchCarouselButton").prop("disabled", true);
    }
    autoRefresh();
});

$("#input-form").submit(function(e) {
    e.preventDefault();
    //getCompanyDetails();
});

$("#favorite-button").click(function() {
    if(isFavorite(selectedCompany)) {
        removeFavorite(selectedCompany);
        $("#favorite-button").css("color", "#ffffff");
    } else {
        addFavorite(selectedCompany);
        $("#favorite-button").css("color", "yellow");
    }
});

$("#stockCarousel").on('slid.bs.carousel', function (e) {
    var carouselData = $("#stockCarousel").data('bs.carousel');
    if(carouselData.getItemIndex(carouselData.$element.find('.item.active')) == 1) {
        if($("ul.nav-pills li.active")[0].id == "historical_charts_tab") {
            displayHistoricalChart();
        }
    } else {
        $("#display_historical_chart").html("");
    }
});

$("a[data-toggle='tab']").on('shown.bs.tab', function(e) {
    var target = $(e.target).attr("href");
    if(target == "#historical_charts") {
        displayHistoricalChart();
    } else if(target == "#stock_details") {
        $("#display_historical_chart").html("");
    } else if(target == "#news_feed") {
        $("#display_historical_chart").html("");
    }
});

//Display Favorites
displayFavorites();
function displayFavorites() {
    $("#favorite-table").find("tr:gt(0)").remove();
    var favorites = getFavorites();
    for(var i = 0; i<favorites.length; i++) {
        var addRow = "<tr id='company-favorite-" + favorites[i] + "'>" +
            "<td><a style='cursor: pointer;' onclick=getCompanyDetails('" + favorites[i] + "')>" + favorites[i] + "</a></td>" +
            "<td id='name-favorite-" + favorites[i] + "'></td>" +
            "<td id='lastPrice-favorite-" + favorites[i] + "'></td>" +
            "<td id='change-favorite-" + favorites[i] + "'></td>" +
            "<td id='marketCap-favorite-" + favorites[i] + "'></td>" +
            "<td id='delete-favorite-" + favorites[i] + "'><button type='button' class='btn btn-default' onclick=removeFavoriteFromTable('" + favorites[i] + "');><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td></tr>";
        $("#favorite-table tr:last").after(addRow);
    }
    for(var i = 0; i<favorites.length; i++) {
        fetchFavoriteDetail(favorites[i], function (details) {
            $("#name-favorite-" + details.Symbol).html(details.Name);
            $("#lastPrice-favorite-" + details.Symbol).html("$ " + details.LastPrice);
            if (details.Change < 0 || details.ChangePercent < 0) {
                $("#change-favorite-" + details.Symbol).html(details.Change + " ( " + details.ChangePercent + "% )<img class='profit-loss-thumbnail' src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png'>").css('color', '#ff0000');
            } else if (details.Change > 0 || details.ChangePercent > 0) {
                $("#change-favorite-" + details.Symbol).html(details.Change + " ( " + details.ChangePercent + "% )<img class='profit-loss-thumbnail' src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png'>").css('color', 'green');
            } else {
                $("#change-favorite-" + details.Symbol).html(details.Change + " ( " + details.ChangePercent + "% )");
            }
            $("#marketCap-favorite-" + details.Symbol).html(details.MarketCap);
        });
    }
}

$(".fb-logo").click(function() {
    FB.ui({
            method: 'feed',
            link: "http://dev.markitondemand.com",
            name: "Current Stock Price of " + selectedCompanyDetails.Name + " is " + selectedCompanyDetails.LastPrice,
            description: "Stock Information of " + selectedCompanyDetails.Name + " (" + selectedCompany + ")",
            picture: selectedCompanyDetails.Image,
            caption: "LAST TRADE PRICE: " + selectedCompanyDetails.LastPrice + ", CHANGE: " + selectedCompanyDetails.Change
        }, function(response){
            if(response) {
                if(response.post_id) {
                    alert("Posted Successfully");
                } else {
                    alert("Not Posted");
                }
            } else {
                alert("Not Posted");
            }
        }
    );
});

function removeFavoriteFromTable(company) {
    $("#company-favorite-" + company).remove();
    removeFavorite(company);
}

function displayStockDetailsCarousel() {
    if(selectedCompany != '') {
        $('#stockCarousel').carousel('next');
    } else {
        alert("No Company Selected.");
    }
}

function clearAll() {
    $("#companySearch").removeAttr('required');
    $("#companySearch").val("");
    $("#invalid-input").css('visibility', 'hidden');
    $("#stockCarousel").carousel(0);
    displayFavorites();
    $("#switchCarouselButton").prop("disabled", true);
    enableCarouselSwitch = false;
    $("#companySearch").prop('required', true);
}
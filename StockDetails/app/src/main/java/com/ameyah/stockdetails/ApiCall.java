package com.ameyah.stockdetails;

/**
 * Created by Ameya on 4/11/2016.
 */

import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;

public class ApiCall {
    private static final String MODLookup = "http://webtech-stock-details.appspot.com/?lookup=";
    private static final String MODDetails = "http://webtech-stock-details.appspot.com/?details=";
    private static final String MODNews = "http://webtech-stock-details.appspot.com/?get_news=";
    private static final String MODFavorite = "http://webtech-stock-details.appspot.com/?favorite_details=";

    public ApiCall() {};
    public String ApiCall(String apiType, String parameter) {
        URL url = null;
        String APIResponse = null;
        try {
            switch (apiType) {
                case "Lookup":
                    url = new URL(MODLookup + parameter);
                    break;
                case "Details":
                    url = new URL(MODDetails + parameter);
                    break;
                case "News":
                    url = new URL(MODNews + parameter);
                    break;
                case "Favorite":
                    url = new URL(MODFavorite + parameter);
            }
            assert url != null;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            Log.d("ab", conn.getInputStream().toString());
            if(conn.getResponseCode() != 200) {
                return null;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            APIResponse = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return APIResponse;
    }
}

package com.ameyah.stockdetails;

/**
 * Created by Ameya on 4/11/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends ArrayAdapter<StockAutoComplete> {
    private List<StockAutoComplete> companyList;

    public AutoCompleteAdapter(Context context, int textViewResourceId, ArrayList<StockAutoComplete> companies) {
        super(context, textViewResourceId, companies);
        this.companyList = companies;
    }

    @Override
    public int getCount() {
        return companyList.size();
    }

    @Override
    public StockAutoComplete getItem(int index) {
        return companyList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter companyListFilter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                JsonParse jp = new JsonParse();

                if (constraint != null) {
                    if (constraint.toString().length() >= 3) {
                        stockSearch.isValidSymbol = false;
                        List<StockAutoComplete> newCompanyList = jp.getCompanyList(constraint.toString());
                        companyList.clear();
                        for (int i = 0; i < newCompanyList.size(); i++) {
                            companyList.add(new StockAutoComplete(newCompanyList.get(i).getCompanySymbol(), newCompanyList.get(i).getCompanyDetails()));
                        }

                        // Now assign the values and count to the FilterResults
                        // object
                        filterResults.values = companyList;
                        filterResults.count = companyList.size();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return companyListFilter;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_company, null);
        }

		/*
         * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */
        StockAutoComplete i = companyList.get(position);

        if (i != null) {

            // This is how you obtain a reference to the TextViews.
            // These TextViews are created in the XML files we defined.

            TextView symbol = (TextView) v.findViewById(R.id.companySymbol);
            TextView details = (TextView) v.findViewById(R.id.companyDetails);

            // check to see if each individual textview is null.
            // if not, assign some text!
            if (symbol != null) {
                symbol.setText(i.getCompanySymbol());
            }
            if (details != null) {
                details.setText(i.getCompanyDetails());
            }
        }

        // the view must be returned to our activity
        return v;

    }
}

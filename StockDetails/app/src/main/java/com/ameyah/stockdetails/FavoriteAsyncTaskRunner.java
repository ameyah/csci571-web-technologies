package com.ameyah.stockdetails;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ameya on 5/4/2016.
 */
public class FavoriteAsyncTaskRunner extends AsyncTask<String, Void, FavoriteListAdapter> {

    private Activity activityContext;
    private ProgressBar spinningLoader;
    private ArrayList<FavoriteDetails> favoriteDetailsList = new ArrayList<FavoriteDetails>();
    public static FavoriteListAdapter favoriteListAdapter;
    private ListView lv;

    public FavoriteAsyncTaskRunner(Activity context) {
        this.activityContext = context;
        ProgressBar favoriteSpinner = (ProgressBar) context.findViewById(R.id.spinningLoaderFavorites);
        ListView lv = (ListView) context.findViewById(R.id.list_favorites);
        this.spinningLoader = favoriteSpinner;
        this.lv = lv;
    }

    @Override
    protected FavoriteListAdapter doInBackground(String... params) {
        JsonParse jp = new JsonParse();
        ArrayList<String> favoriteList = FavoriteList.getPreferences(activityContext);

        if(params[0] == "removeFavorite1") {
            FavoriteDetails favoriteObject = favoriteListAdapter.getItem(Integer.parseInt(params[1]));
            favoriteListAdapter.remove(favoriteObject);
            return favoriteListAdapter;
        } else {
            if (!favoriteList.isEmpty()) {
                for (String favorite : favoriteList) {
                    if (favorite != null || favorite != "") {
                        JSONObject favoriteDetailsJSON = null;
                        try {
                            Log.d("favorite", favorite);
                            favoriteDetailsJSON = jp.getFavoriteDetails(favorite);
                        } catch (NullPointerException e) {

                        }
                        FavoriteDetails favoriteDetails = new FavoriteDetails(favoriteDetailsJSON);
                        this.favoriteDetailsList.add(favoriteDetails);
                    }
                }
            }
            favoriteListAdapter = new FavoriteListAdapter(activityContext, R.layout.list_favorites, favoriteDetailsList);
            return favoriteListAdapter;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(FavoriteListAdapter favoriteListAdapter) {
        // execution of result of Long time consuming operation
        super.onPostExecute(favoriteListAdapter);
        lv.setAdapter(this.favoriteListAdapter);
        spinningLoader.setVisibility(View.INVISIBLE);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {
        // Things to be done before execution of long running operation. For
        // example showing ProgessDialog
        super.onPreExecute();
        spinningLoader.setVisibility(View.VISIBLE);
    }
}

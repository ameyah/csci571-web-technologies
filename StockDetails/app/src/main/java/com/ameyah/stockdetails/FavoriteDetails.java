package com.ameyah.stockdetails;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ameya on 5/3/2016.
 */
public class FavoriteDetails {

    String name;
    String symbol;
    String lastPrice;
    String changePercent;
    String marketCap;

    public FavoriteDetails(JSONObject details){
        try {
            this.setCompanyName(details.get("Name").toString());
            this.setCompanySymbol(details.get("Symbol").toString());
            this.setCompanyLastPrice(details.get("LastPrice").toString());
            this.setCompanyChangePercent(details.get("ChangePercent").toString());
            this.setCompanyMarketCap(details.get("MarketCap").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //setters
    public void setCompanyName(String name) {
        this.name = name;
    }
    public void setCompanySymbol(String symbol) {
        this.symbol = symbol;
    }
    public void setCompanyChangePercent(String changePercent) {
        this.changePercent = changePercent;
    }
    public void setCompanyLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }
    public void setCompanyMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    //getters
    public String getCompanyName() {
        return this.name;
    }
    public String getCompanySymbol() {
        return this.symbol;
    }
    public String getCompanyLastPrice() {
        return this.lastPrice;
    }
    public String getCompanyChangePercent() {
        return this.changePercent;
    }
    public String getCompanyMarketCap() {
        return this.marketCap;
    }
}

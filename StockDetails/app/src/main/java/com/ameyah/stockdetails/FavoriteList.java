package com.ameyah.stockdetails;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Created by Ameya on 5/3/2016.
 */
public class FavoriteList {

    public static ArrayList<String> getPreferences(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return new ArrayList(Arrays.asList(preferences.getString("favorite_list", "").split(",")));
    }

    public static Boolean isEmpty(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(preferences.getString("favorite_list", "") == "") {
            return true;
        }
        return false;
    }

    public static void addPreference(Context context, String favorite) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String favorite_list = preferences.getString("favorite_list", "");
        if(favorite_list.isEmpty()) {
            preferences.edit().putString("favorite_list", favorite).apply();
        } else {
            preferences.edit().putString("favorite_list", String.format("%s,%s", favorite_list, favorite)).apply();
        }
    }

    public static boolean isFavorite(Context context, String favorite) {
        return getPreferences(context).contains(favorite);
    }

    public static void removeFavorite(Context context, String favorite) {
        ArrayList<String> favorite_list = getPreferences(context);
        if(!favorite_list.contains(favorite)) {
            return;
        }
        for (Iterator<String> iter = favorite_list.listIterator(); iter.hasNext(); ) {
            String a = iter.next();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Objects.equals(a, favorite)) {
                    iter.remove();
                }
            }
        }
        String newFavoriteList = "";
        for(String fav: favorite_list) {
            newFavoriteList += fav + ",";
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(newFavoriteList == "") {
            preferences.edit().putString("favorite_list", "").apply();
        } else {
            preferences.edit().putString("favorite_list", newFavoriteList.substring(0, (newFavoriteList != null ? newFavoriteList.length() : 0) - 1)).apply();
        }
    }

}

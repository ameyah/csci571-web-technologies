package com.ameyah.stockdetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ameya on 5/3/2016.
 */

public class FavoriteListAdapter extends ArrayAdapter<FavoriteDetails> {
    private ArrayList<FavoriteDetails> FavoriteDetailsList;
    private Boolean render;

    /* here we must override the constructor for ArrayAdapter
    * the only variable we care about now is ArrayList<Item> objects,
    * because it is the list of objects we want to display.
    */
    public FavoriteListAdapter(Context context, int textViewResourceId, ArrayList<FavoriteDetails> objects) {
        super(context, textViewResourceId, objects);
        this.FavoriteDetailsList = objects;
        this.render = false;
    }

    @Override
    public int getCount() {
        return FavoriteDetailsList.size();
    }

    @Override
    public FavoriteDetails getItem(int index) {
        return FavoriteDetailsList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter FavoriteDetailsFilter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if (constraint != null) {
                    // Now assign the values and count to the FilterResults
                    // object
                    filterResults.values = FavoriteDetailsList;
                    filterResults.count = FavoriteDetailsList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return FavoriteDetailsFilter;
    }

    /*
     * we are overriding the getView method here - this is what defines how each
     * list item will look.
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_favorites, null);
        }

		/*
         * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */
        FavoriteDetails i = FavoriteDetailsList.get(position);

        if (i != null) {
            if (i.getCompanySymbol() != null) {
                // This is how you obtain a reference to the TextViews.
                // These TextViews are created in the XML files we defined.

                TextView companySymbol = (TextView) v.findViewById(R.id.companySymbol);
                TextView companyName = (TextView) v.findViewById(R.id.companyName);
                TextView companyLastPrice = (TextView) v.findViewById(R.id.companyLastPrice);
                TextView companyMarketCap = (TextView) v.findViewById(R.id.companyMarketCap);
                TextView companyChangePercent = (TextView) v.findViewById(R.id.companyChangePercent);

                if (companySymbol != null) {
                    companySymbol.setText(i.getCompanySymbol());
                }
                if (companyName != null) {
                    companyName.setText(i.getCompanyName());
                }
                if (companyLastPrice != null) {
                    companyLastPrice.setText("$" + i.getCompanyLastPrice());
                }
                if (companyChangePercent != null) {
                    String changePercent = i.getCompanyChangePercent();
                    if(changePercent.contains("-")) {
                        companyChangePercent.setText(changePercent + "%");
                        companyChangePercent.setBackgroundColor(v.getResources().getColor(R.color.red));
                    } else {
                        companyChangePercent.setText("+" + changePercent + "%");
                        companyChangePercent.setBackgroundColor(v.getResources().getColor(R.color.green));
                    }
                }
                if (companyMarketCap != null) {
                    companyMarketCap.setText("Market Cap: " + i.getCompanyMarketCap());
                }


            }
        }

        // the view must be returned to our activity
        return v;

    }
}
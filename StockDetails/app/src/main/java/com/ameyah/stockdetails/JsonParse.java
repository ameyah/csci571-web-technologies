package com.ameyah.stockdetails;

/**
 * Created by Ameya on 4/11/2016.
 */

import android.view.View;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonParse {
    String company;
    public JsonParse() {}
    public JsonParse(String company) {
        this.company = company;
    }

    public List<StockAutoComplete> getCompanyList(String searchText) {
        List<StockAutoComplete> companyList = new ArrayList<StockAutoComplete>();
        try {
            String formattedSearchText = searchText.replace(" ", "%20");
            ApiCall APIObject = new ApiCall();
            String APIResponse = APIObject.ApiCall("Lookup", formattedSearchText);
            JSONArray jsonResponse = new JSONArray(APIResponse);
            for(int i = 0; i < jsonResponse.length(); i++){
                String responseString = jsonResponse.getString(i);
                companyList.add(new StockAutoComplete(responseString.split(" - ")[0], responseString.split("-")[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return companyList;
    }

    public String getCompanyDetails(String companySymbol) {
//        List<stockDetails> companyDetails = new ArrayList<stockDetails>();
        JSONObject jsonResponse = new JSONObject();
        try {
            ApiCall APIObject = new ApiCall();
            String APIResponse = APIObject.ApiCall("Details", companySymbol);
            if(APIResponse == null) {
                return null;
            }
            jsonResponse = new JSONObject(APIResponse);
//            companyDetails.add(new stockDetails(jsonResponse));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResponse.toString();
    }

    public List<StockNews> getCompanyNews(String companySymbol) {
        List<StockNews> NewsList = new ArrayList<StockNews>();
        try {
            String formattedCompanySymbol = companySymbol.replace(" ", "%20");
            ApiCall APIObject = new ApiCall();
            String APIResponse = APIObject.ApiCall("News", formattedCompanySymbol);
            JSONArray jsonResponse = new JSONArray(APIResponse);
            for(int i = 0; i < jsonResponse.length(); i++){
                JSONObject singleNewsJSON = jsonResponse.getJSONObject(i);
                NewsList.add(new StockNews(singleNewsJSON));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return NewsList;
    }

    public JSONObject getFavoriteDetails(String companySymbol) {
        JSONObject favoriteDetails = new JSONObject();
        try {
            ApiCall APIObject = new ApiCall();
            String APIResponse = APIObject.ApiCall("Favorite", companySymbol);
            favoriteDetails = new JSONObject(APIResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return favoriteDetails;
    }

    /*public List<StockNews> getCompanyNews(String companySymbol) {
        List<StockNews> NewsList = new ArrayList<StockNews>();
        try {
            String formattedCompanySymbol = companySymbol.replace(" ", "%20");
            ApiCall APIObject = new ApiCall();
            String APIResponse = APIObject.ApiCall("News", formattedCompanySymbol);
            JSONArray jsonResponse = new JSONArray(APIResponse);
            for(int i = 0; i < jsonResponse.length(); i++){
                JSONObject singleNewsJSON = jsonResponse.getJSONObject(i);
                NewsList.add(new StockNews(singleNewsJSON));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return NewsList;
    }*/


}

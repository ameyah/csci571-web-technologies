package com.ameyah.stockdetails;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import android.app.ListActivity;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookDialog;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import uk.co.senab.photoview.PhotoViewAttacher;

public class StockDetailsActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private CustomViewPager mViewPager;
    public stockDetails companyDetails;
    private View current_saved = null;
    private View historical_saved = null;
    private View news_saved = null;

    private boolean currentViewFlag = false;
    private boolean historicalViewFlag = false;
    private boolean newsViewFlag = false;

    PhotoViewAttacher yahoo_image_zoom;

    CallbackManager callbackManager;
    ShareDialog shareDialog = new ShareDialog(this);
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_details);
        c = this;

        /*toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOnBackPress();
            }
        });*/
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (com.ameyah.stockdetails.CustomViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPagingEnabled(false);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Intent intent = getIntent();
        String companyDetailsStr = intent.getStringExtra("companyDetailsStr");
        JSONObject companyDetailsJson = new JSONObject();
        try {
            companyDetailsJson = new JSONObject(companyDetailsStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        companyDetails = new stockDetails(companyDetailsJson);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        getSupportActionBar().setTitle(companyDetails.getCompanyName());

        /*callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

                Log.d("fb", "msg" + result.getPostId());
                if (result.getPostId() == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(c);
                    builder.setMessage("Not posted on Facebook")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(c);
                    builder.setMessage("Posted successfully on FB")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }

            @Override
            public void onCancel() {
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setMessage("Not posted on Facebook")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }

            @Override
            public void onError(FacebookException error) {

            }
        });*/

    }

    /*@Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
            return super.onOptionsItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.action_fb:
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(String.format("http://finance.yahoo.com/q?s=%s", companyDetails.getCompanySymbol())))
                        .setImageUrl(Uri.parse(String.format("http://chart.finance.yahoo.com/t?s=%s&lang=en-US&width=200&height=200", companyDetails.getCompanySymbol())))
                        .setContentTitle(String.format("Stock Value of %s is $ %s", companyDetails.getCompanyName(), companyDetails.getCompanyLastPrice()))
                        .setContentDescription(String.format("Stock information of %s", companyDetails.getCompanyName()))
                        .build();
                shareDialog.show(content);
                break;
            case R.id.action_favorite:
                if (FavoriteList.isFavorite(this, companyDetails.getCompanySymbol())) {
//                    stockSearch.favorite_list.remove(companyDetails.getCompanySymbol());
                    FavoriteList.removeFavorite(this, companyDetails.getCompanySymbol());
                    item.setIcon(android.R.drawable.star_big_off);
                    Toast.makeText(getApplicationContext(), String.format("\"%s\" removed from bookmarks", companyDetails.getCompanyName()), Toast.LENGTH_SHORT).show();
                } else {
//                    stockSearch.favorite_list.add(companyDetails.getCompanySymbol());
                    FavoriteList.addPreference(this, companyDetails.getCompanySymbol());
                    item.setIcon(android.R.drawable.star_big_on);
                    Toast.makeText(getApplicationContext(), String.format("Bookmarked \"%s\"!", companyDetails.getCompanyName()), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
        return false;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem favorite_button = menu.findItem(R.id.action_favorite);
        if (FavoriteList.isFavorite(this, companyDetails.getCompanySymbol())) {
            assert favorite_button != null;
            favorite_button.setIcon(android.R.drawable.star_big_on);
        } else {
            assert favorite_button != null;
            favorite_button.setIcon(android.R.drawable.star_big_off);
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stock_details, menu);
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private ArrayList<StockNews> news_list = new ArrayList<StockNews>();


        private ArrayList<StockNews> getNews(String companySymbol) {
            JsonParse jp = new JsonParse();
            List<StockNews> newNewsList = jp.getCompanyNews(companySymbol);
            news_list.clear();
            for (int i = 0; i < newNewsList.size(); i++) {
                JSONObject newsInfo = new JSONObject();
                try {
                    newsInfo.put("title", newNewsList.get(i).getNewsTitle());
                    newsInfo.put("url", newNewsList.get(i).getNewsUrl());
                    newsInfo.put("description", newNewsList.get(i).getNewsDescription());
                    newsInfo.put("publisher", newNewsList.get(i).getNewsPublisher());
                    newsInfo.put("date", newNewsList.get(i).getNewsDate());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                news_list.add(new StockNews(newsInfo));
            }
            return news_list;
        }

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            final StockDetailsActivity activityContext = (StockDetailsActivity) getActivity();
            View rootView_current = null;
            View rootView_historical = null;
            View rootView_news = null;
            rootView_current = inflater.inflate(R.layout.fragment_stock_details_current, container, false);
            rootView_historical = inflater.inflate(R.layout.fragment_stock_details_historical, container, false);
            rootView_news = inflater.inflate(R.layout.fragment_stock_details_news, container, false);
            /*if(!activityContext.currentViewFlag) {
                rootView_current = inflater.inflate(R.layout.fragment_stock_details_current, container, false);
            } else {
                return activityContext.current_saved;
            }
            if(!activityContext.historicalViewFlag) {
                rootView_historical = inflater.inflate(R.layout.fragment_stock_details_historical, container, false);
            } else {
                return activityContext.historical_saved;
            }
            if(!activityContext.newsViewFlag) {
                rootView_news = inflater.inflate(R.layout.fragment_stock_details_news, container, false);
            } else {
                return activityContext.news_saved;
            }*/
            View rootView = null;

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    TextView name = (TextView) rootView_current.findViewById(R.id.stock_name);
                    name.setText((CharSequence) activityContext.companyDetails.getCompanyName());

                    TextView symbol = (TextView) rootView_current.findViewById(R.id.stock_symbol);
                    symbol.setText((CharSequence) activityContext.companyDetails.getCompanySymbol());

                    TextView lastPrice = (TextView) rootView_current.findViewById(R.id.stock_last_price);
                    lastPrice.setText((CharSequence) activityContext.companyDetails.getCompanyLastPrice());

                    String companyChange = activityContext.companyDetails.getCompanyChange();
                    TextView change = (TextView) rootView_current.findViewById(R.id.stock_change);
                    if (companyChange.contains("-")) {
                        rootView_current.findViewById(R.id.stock_change_image).setBackgroundResource(R.drawable.down);
                        change.setText(String.format("%s", (CharSequence) companyChange));
                    } else {
                        rootView_current.findViewById(R.id.stock_change_image).setBackgroundResource(R.drawable.up);
                        change.setText(String.format("%s", (CharSequence) companyChange));
                    }

                    TextView timeStamp = (TextView) rootView_current.findViewById(R.id.stock_timestamp);
                    timeStamp.setText((CharSequence) activityContext.companyDetails.getCompanyTimeDate());

                    TextView marketCap = (TextView) rootView_current.findViewById(R.id.stock_market_cap);
                    marketCap.setText((CharSequence) activityContext.companyDetails.getCompanyMarketCap());

                    TextView volume = (TextView) rootView_current.findViewById(R.id.stock_volume);
                    volume.setText((CharSequence) activityContext.companyDetails.getCompanyVolume());

                    String companyChangeYTD = activityContext.companyDetails.getCompanyChangeYTD();
                    TextView changeYTD = (TextView) rootView_current.findViewById(R.id.stock_change_ytd);
                    if (companyChangeYTD.contains("-")) {
                        rootView_current.findViewById(R.id.stock_change_ytd_image).setBackgroundResource(R.drawable.down);
                        changeYTD.setText(String.format("%s", (CharSequence) companyChangeYTD));
                    } else {
                        rootView_current.findViewById(R.id.stock_change_ytd_image).setBackgroundResource(R.drawable.up);
                        changeYTD.setText(String.format("%s", (CharSequence) companyChangeYTD));
                    }

                    TextView highPrice = (TextView) rootView_current.findViewById(R.id.stock_high_price);
                    highPrice.setText((CharSequence) activityContext.companyDetails.getCompanyHighPrice());

                    TextView lowPrice = (TextView) rootView_current.findViewById(R.id.stock_low_price);
                    lowPrice.setText((CharSequence) activityContext.companyDetails.getCompanyLowPrice());

                    TextView openPrice = (TextView) rootView_current.findViewById(R.id.stock_open_price);
                    openPrice.setText((CharSequence) activityContext.companyDetails.getCompanyOpeningPrice());

                    ImageView yahoo_image = (ImageView) rootView_current.findViewById(R.id.stock_yahoo_image);
                    URL imageUrl = null;
                    try {
                        imageUrl = new URL("http://chart.finance.yahoo.com/t?s=" + activityContext.companyDetails.getCompanySymbol() + "&lang=en-US");
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    Bitmap mIcon_val = null;
                    try {
                        mIcon_val = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    yahoo_image.setImageBitmap(mIcon_val);
//                    activityContext.current_saved = rootView_current;
//                    activityContext.currentViewFlag = true;
                    rootView = rootView_current;
                    break;

                case 2:
//                    setContentView(R.layout.fragment_stock_details_historical);
                    WebView mWebView = null;
                    mWebView = (WebView) rootView_historical.findViewById(R.id.historical_webview);
                    mWebView.getSettings().setJavaScriptEnabled(true);
                    mWebView.loadUrl("file:///android_asset/historical.html?" + activityContext.companyDetails.getCompanySymbol());
//                    activityContext.historical_saved = rootView_historical;
//                    activityContext.historicalViewFlag = true;
                    rootView = rootView_historical;
                    break;

                case 3:
                    ArrayList<StockNews> news_list = getNews(activityContext.companyDetails.getCompanySymbol());
                    StockNewsAdapter adapter1 = new StockNewsAdapter(activityContext, R.layout.list_stock_news, news_list);

                    ListView lv = (ListView) rootView_news.findViewById(R.id.list_stock_news);
                    lv.setOnItemClickListener(new onItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapter, View v, int position,
                                                long arg3) {
                            StockNews news = (StockNews) adapter.getItemAtPosition(position);
                            String urlString = news.getNewsUrl();
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setPackage("com.android.chrome");
                            try {
                                activityContext.startActivity(intent);
                            } catch (ActivityNotFoundException ex) {
                                // Chrome browser presumably not installed so allow user to choose instead
                                intent.setPackage(null);
                                activityContext.startActivity(intent);
                            }
                        }
                    });

                    lv.setAdapter(adapter1);


                    rootView = rootView_news;
                    break;
            }
            /*TextView textView = (TextView) rootView_current.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));*/


            return rootView;
        }

        private class onItemClickListener implements AdapterView.OnItemClickListener {
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {

            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "CURRENT";
                case 1:
                    return "HISTORICAL";
                case 2:
                    return "NEWS";
            }
            return null;
        }
    }

    public Boolean renderYahooImage(View v) {
        final ImageView yahoo_image = (ImageView) findViewById(R.id.stock_yahoo_image);

        URL imageUrl = null;
        try {
            imageUrl = new URL("http://chart.finance.yahoo.com/t?s=" + companyDetails.getCompanySymbol() + "&lang=en-US&width=520&height=400");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Bitmap mIcon_val = null;
        try {
            mIcon_val = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }


        final Bitmap yahoo_image_zoom_image = mIcon_val;

        if (yahoo_image != null) {
            yahoo_image.bringToFront();
            yahoo_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(v.getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow()
                            .setSoftInputMode(
                                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                                            | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    dialog.setContentView(R.layout.image_zoom_layout);
                    ImageView image = (ImageView) dialog.findViewById(R.id.image);
                    PhotoViewAttacher mAttacher = new PhotoViewAttacher(image);
                    image.setImageBitmap(yahoo_image_zoom_image);
                    dialog.show();
                    mAttacher.update();
                }
            });
        }

        return true;

    }


}

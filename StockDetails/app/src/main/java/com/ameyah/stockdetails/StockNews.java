package com.ameyah.stockdetails;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ameya on 5/3/2016.
 */
public class StockNews {
    String title, url, description, publisher, date;

    public StockNews(JSONObject newsInfo){
        try {
            this.setNewsTitle(newsInfo.get("title").toString());
            this.setNewsUrl(newsInfo.get("url").toString());
            this.setNewsDescription(newsInfo.get("description").toString());
            this.setNewsPublisher(newsInfo.get("publisher").toString());
            this.setNewsDate(newsInfo.get("date").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String getNewsTitle() {
        return this.title;
    }
    public String getNewsDescription() {
        return this.description;
    }
    public String getNewsUrl() {
        return this.url;
    }
    public String getNewsPublisher() {
        return this.publisher;
    }
    public String getNewsDate() {
        return this.date;
    }

    public void setNewsTitle(String title) {
        this.title = title;
    }
    public void setNewsUrl(String url) {
        this.url = url;
    }
    public void setNewsDescription(String description) {
        this.description = description;
    }
    public void setNewsPublisher(String publisher) {
        this.publisher = publisher;
    }
    public void setNewsDate(String date) {
        this.date = date;
    }

}

package com.ameyah.stockdetails;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ameya on 5/3/2016.
 */

public class StockNewsAdapter extends ArrayAdapter<StockNews> {
    private ArrayList<StockNews> News;

    /* here we must override the constructor for ArrayAdapter
    * the only variable we care about now is ArrayList<Item> objects,
    * because it is the list of objects we want to display.
    */
    public StockNewsAdapter(Context context, int textViewResourceId, ArrayList<StockNews> objects) {
        super(context, textViewResourceId, objects);
        this.News = objects;
    }

    @Override
    public int getCount() {
        return News.size();
    }

    @Override
    public StockNews getItem(int index) {
        return News.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter NewsFilter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                JsonParse jp = new JsonParse();

                if (constraint != null) {
                    /*List<StockNews> newNewsList = jp.getCompanyNews(constraint.toString());
                    News.clear();
                    for (int i = 0; i < newNewsList.size(); i++) {
                        JSONObject newsInfo = new JSONObject();
                        try {
                            newsInfo.put("title", newNewsList.get(i).getNewsTitle());
                            newsInfo.put("url", newNewsList.get(i).getNewsUrl());
                            newsInfo.put("description", newNewsList.get(i).getNewsDescription());
                            newsInfo.put("publisher", newNewsList.get(i).getNewsPublisher());
                            newsInfo.put("date", newNewsList.get(i).getNewsDate());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        News.add(new StockNews(newsInfo));
                    }*/

                    // Now assign the values and count to the FilterResults
                    // object
                    filterResults.values = News;
                    filterResults.count = News.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return NewsFilter;
    }

    /*
     * we are overriding the getView method here - this is what defines how each
     * list item will look.
     */
    public View getView(int position, View convertView, ViewGroup parent){

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_stock_news, null);
        }

		/*
		 * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */
        StockNews i = News.get(position);

        if (i != null) {

            // This is how you obtain a reference to the TextViews.
            // These TextViews are created in the XML files we defined.

            TextView newsTitle = (TextView) v.findViewById(R.id.newsTitle);
            TextView newsDescription = (TextView) v.findViewById(R.id.newsDescription);
            TextView newsPublisher = (TextView) v.findViewById(R.id.newsPublisher);
            TextView newsDate = (TextView) v.findViewById(R.id.newsDate);

            // check to see if each individual textview is null.
            // if not, assign some text!
            if (newsTitle != null){
                SpannableString title = new SpannableString(i.getNewsTitle());
                title.setSpan(new UnderlineSpan(), 0, title.length(), 0);
                newsTitle.setText(title);
            }
            if (newsDescription != null){
                newsDescription.setText(i.getNewsDescription());
            }
            if (newsPublisher != null){
                newsPublisher.setText("Publisher: " + i.getNewsPublisher());
            }
            if (newsDate != null){
                newsDate.setText("Date: " + i.getNewsDate());
            }
        }

        // the view must be returned to our activity
        return v;

    }
}

package com.ameyah.stockdetails;

/**
 * Created by Ameya on 4/11/2016.
 */
public class StockAutoComplete {
    String companySymbol;
    String companyDetails;
    public StockAutoComplete(String symbol, String details){
        this.setCompanySymbol(symbol);
        this.setCompanyDetails(details);
    }
    public String getCompanySymbol() {
        return companySymbol;
    }
    public String getCompanyDetails() {
        return companyDetails;
    }
    public void setCompanySymbol(String symbol) {
        this.companySymbol = symbol;
    }
    public void setCompanyDetails(String details) {
        this.companyDetails = details;
    }
}

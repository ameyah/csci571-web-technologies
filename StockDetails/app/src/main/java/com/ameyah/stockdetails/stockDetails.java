package com.ameyah.stockdetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Ameya on 5/1/2016.
 */
public class stockDetails {
    String status;
    String name;
    String symbol;
    String lastPrice;
    String change;
    String timeDate;
    String marketCap;
    String volume;
    String changeYTD;
    String highPrice;
    String lowPrice;
    String openingPrice;

    public stockDetails(JSONObject details){
        try {
            this.setCompanyStatus(details.get("Status").toString());
            this.setCompanyName(details.get("Name").toString());
            this.setCompanySymbol(details.get("Symbol").toString());
            this.setCompanyLastPrice(details.get("LastPrice").toString());
            if(details.get("ChangePercent").toString().contains("-")) {
                this.setCompanyChange(details.get("Change").toString() + " (" + details.get("ChangePercent").toString() + "%)");
            } else {
                this.setCompanyChange(details.get("Change").toString() + " (+" + details.get("ChangePercent").toString() + "%)");
            }
            this.setCompanyTimeDate(details.get("Timestamp").toString());
            this.setCompanyMarketCap(details.get("MarketCap").toString());
            this.setCompanyVolume(details.get("Volume").toString());
            if(details.get("ChangePercentYTD").toString().contains("-")) {
                this.setCompanyChangeYTD(details.get("ChangeYTD").toString() + " (" + details.get("ChangePercentYTD").toString() + "%)");
            } else {
                this.setCompanyChangeYTD(details.get("ChangeYTD").toString() + " (+" + details.get("ChangePercentYTD").toString() + "%)");
            }
            this.setCompanyHighPrice(details.get("High").toString());
            this.setCompanyLowPrice(details.get("Low").toString());
            this.setCompanyOpeningPrice(details.get("Open").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //setters
    public void setCompanyStatus(String status) {
        this.status = status;
    }
    public void setCompanyName(String name) {
        this.name = name;
    }
    public void setCompanySymbol(String symbol) {
        this.symbol = symbol;
    }
    public void setCompanyLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }
    public void setCompanyChange(String change) {
        this.change = change;
    }
    public void setCompanyTimeDate(String timeDate) {
        this.timeDate = timeDate;
    }
    public void setCompanyMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }
    public void setCompanyVolume(String volume) {
        this.volume = volume;
    }
    public void setCompanyChangeYTD(String changeYTD) {
        this.changeYTD = changeYTD;
    }
    public void setCompanyHighPrice(String highPrice) {
        this.highPrice = highPrice;
    }
    public void setCompanyLowPrice(String lowPrice) {
        this.lowPrice = lowPrice;
    }
    public void setCompanyOpeningPrice(String openingPrice) {
        this.openingPrice = openingPrice;
    }

    //getters
    public String getCompanyStatus() {
        return this.status;
    }
    public String getCompanyName() {
        return this.name;
    }
    public String getCompanySymbol() {
        return this.symbol;
    }
    public String getCompanyLastPrice() {
        return this.lastPrice;
    }
    public String getCompanyChange() {
        return this.change;
    }
    public String getCompanyTimeDate() {
        return this.timeDate;
    }
    public String getCompanyMarketCap() {
        return this.marketCap;
    }
    public String getCompanyVolume() {
        return this.volume;
    }
    public String getCompanyChangeYTD() {
        return this.changeYTD;
    }
    public String getCompanyHighPrice() {
        return this.highPrice;
    }
    public String getCompanyLowPrice() {
        return this.lowPrice;
    }
    public String getCompanyOpeningPrice() {
        return this.openingPrice;
    }
}

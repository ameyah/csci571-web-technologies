package com.ameyah.stockdetails;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class stockSearch extends AppCompatActivity {
    private ArrayList<StockAutoComplete> company_list = new ArrayList<StockAutoComplete>();
    private ProgressBar spinningLoader;

    public static List<String> favorite_list = new ArrayList<String>();
    public static Boolean isValidSymbol = false;

    private int autoRefreshInterval = 10000;
    private Handler autoRefreshHandler;

    private FavoriteAsyncTaskRunner runner;
    private FavoriteListAdapter favoriteListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_search);
        final CustomAutoCompleteTextView searchStock = (CustomAutoCompleteTextView) findViewById(R.id.stock_search);
        spinningLoader = (ProgressBar) findViewById(R.id.spinningLoader);
        searchStock.setLoadingIndicator(spinningLoader);
        final Button getQuote = (Button) findViewById(R.id.get_quote_button);

        autoRefreshHandler = new Handler();

        AutoCompleteAdapter adapter1 = new AutoCompleteAdapter(this, R.layout.list_company, company_list);
        searchStock.setAdapter(adapter1);

        printSigningKeys(this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        FacebookSdk.sdkInitialize(getApplicationContext());

//        Toast.makeText(getApplicationContext(), ((String) FavoriteList.getPreferences(this).isEmpty()), Toast.LENGTH_LONG).show();
        if (!FavoriteList.isEmpty(this)) {
            ListView lv = (ListView) findViewById(R.id.list_favorites);
            runner = new FavoriteAsyncTaskRunner(stockSearch.this);
            runner.execute("new");
            if (lv != null) {
                lv.setOnItemClickListener(new onItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View v, int position,
                                            long arg3) {
                        FavoriteDetails favoriteDetails = (FavoriteDetails) adapter.getItemAtPosition(position);
                        JsonParse jp = new JsonParse();
                        String companyDetailsStr = jp.getCompanyDetails(favoriteDetails.getCompanySymbol());
                        if (companyDetailsStr == null || companyDetailsStr == "") {
                            String message = "No stock Information Available";
                            showAlert(stockSearch.this, message);
                        } else {
                            isValidSymbol = false;
                            ((com.ameyah.stockdetails.CustomAutoCompleteTextView) findViewById(R.id.stock_search)).setText("");
                            Intent activityChangeIntent = new Intent(stockSearch.this, StockDetailsActivity.class);
                            activityChangeIntent.putExtra("companyDetailsStr", companyDetailsStr);
                            stockSearch.this.startActivity(activityChangeIntent);
                        }
                    }
                });
            }
        }

        ImageButton refreshButton = (ImageButton) findViewById(R.id.refresh_favorite_button);
        if (refreshButton != null) {
            refreshButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    runner = new FavoriteAsyncTaskRunner(stockSearch.this);
                    runner.execute("refresh");

                }
            });
        }

        Switch autoRefreshSwitch = (Switch) findViewById(R.id.refresh_favorite_switch);
        if (autoRefreshSwitch != null) {
            autoRefreshSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        startAutoRefreshTask();
                    } else {
                        stopAutoRefreshTask();
                    }
                }
            });
        }

        Button clearButton = (Button) findViewById(R.id.clear_button);
        if (clearButton != null) {
            clearButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ((EditText) findViewById(R.id.stock_search)).setText("");
                    isValidSymbol = false;
                }
            });
        }

        searchStock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EditText stockSearch = (EditText) findViewById(R.id.stock_search);
                stockSearch.setText(company_list.get(position).getCompanySymbol());
                stockSearch.setSelection(stockSearch.getText().length());
                isValidSymbol = true;
            }
        });

        DynamicListView favorite_list_listview = (DynamicListView) findViewById(R.id.list_favorites);
        if (favorite_list_listview != null) {
            favorite_list_listview.enableSwipeToDismiss(
                    new OnDismissCallback() {
                        @Override
                        public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions) {

                            Log.d("swipe", String.valueOf(reverseSortedPositions[0]));
                            /*String[] params = new String[2];
                            params[0] = "removeFavorite";
                            params[1] = String.valueOf(reverseSortedPositions[0]);
                            runner = new FavoriteAsyncTaskRunner(stockSearch.this);
                            runner.execute(params);*/
                            FavoriteDetails favoriteObject = FavoriteAsyncTaskRunner.favoriteListAdapter.getItem(reverseSortedPositions[0]);
                            showConfirmAlert(stockSearch.this, String.format("Want to delete %s from favorites?", favoriteObject.getCompanyName()), favoriteObject);
                        }
                    }
            );
        }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        assert getQuote != null;
        getQuote.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JsonParse jp = new JsonParse();
                EditText companySymbol = (EditText) findViewById(R.id.stock_search);
                if (companySymbol != null) {
//                    Log.d("symbol", companySymbol.getText().toString());
                    if (companySymbol.getText().toString().matches("")) {
                        String message = "Please enter a Stock Name/Symbol";
                        showAlert(stockSearch.this, message);
                    } else {
                        String companyDetailsStr = jp.getCompanyDetails(companySymbol.getText().toString());
                        if (companyDetailsStr == null || companyDetailsStr == "") {
                            String message = "";
                            Log.d("isvalid", isValidSymbol.toString());
                            if (isValidSymbol) {
                                message = "No Stock Information Available";
                                showAlert(stockSearch.this, message);
                            } else {
                                message = "Invalid Symbol";
                                showAlert(stockSearch.this, message);
                            }
                        } else {
                            Intent activityChangeIntent = new Intent(stockSearch.this, StockDetailsActivity.class);
                            activityChangeIntent.putExtra("companyDetailsStr", companyDetailsStr);
                            stockSearch.this.startActivity(activityChangeIntent);
                        }
                    }

                }
            }
        });


    }

    Runnable autoRefreshStatusChecker = new Runnable() {
        @Override
        public void run() {
            //Refresh Favorite List

            runner = new FavoriteAsyncTaskRunner(stockSearch.this);
            runner.execute("refresh");

            autoRefreshHandler.postDelayed(autoRefreshStatusChecker, autoRefreshInterval);
        }
    };

    void startAutoRefreshTask() {
        autoRefreshStatusChecker.run();
    }

    void stopAutoRefreshTask() {
        autoRefreshHandler.removeCallbacks(autoRefreshStatusChecker);
    }

    public static void printSigningKeys(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static void showAlert(final Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showConfirmAlert(final Context context, String message, final FavoriteDetails favoriteObject) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (favoriteObject != null) {
                            FavoriteAsyncTaskRunner.favoriteListAdapter.remove(favoriteObject);
                            FavoriteList.removeFavorite(context, favoriteObject.getCompanySymbol());
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        runner = new FavoriteAsyncTaskRunner(stockSearch.this);
        runner.execute("new");
    }

    private class onItemClickListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> adapter, View v, int position,
                                long arg3) {

        }
    }
}
